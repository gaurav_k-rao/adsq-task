-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2019 at 05:04 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insights`
--

-- --------------------------------------------------------

--
-- Table structure for table `lead`
--

CREATE TABLE `lead` (
  `id` int(11) NOT NULL,
  `Channel` varchar(255) NOT NULL,
  `Ad Type` varchar(255) NOT NULL,
  `My CPC` float NOT NULL,
  `Benchmark CPC` float NOT NULL,
  `CPC % Difference` varchar(255) NOT NULL,
  `My CPM` float NOT NULL,
  `Benchmark CPM` float NOT NULL,
  `CPM % Difference` varchar(255) NOT NULL,
  `My CPL` float NOT NULL,
  `Benchmark CPL` float NOT NULL,
  `CPL % Difference` varchar(255) NOT NULL,
  `My CTR` float NOT NULL,
  `Benchmark CTR` float NOT NULL,
  `CTR % Difference` varchar(255) NOT NULL,
  `My Cost per ThruPlay` float NOT NULL,
  `Benchmark Cost per ThruPlay` float NOT NULL,
  `Cost per ThruPlay % Difference` varchar(255) NOT NULL,
  `My CPE` float NOT NULL,
  `Benchmark CPE` float NOT NULL,
  `CPE % Difference` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead`
--

INSERT INTO `lead` (`id`, `Channel`, `Ad Type`, `My CPC`, `Benchmark CPC`, `CPC % Difference`, `My CPM`, `Benchmark CPM`, `CPM % Difference`, `My CPL`, `Benchmark CPL`, `CPL % Difference`, `My CTR`, `Benchmark CTR`, `CTR % Difference`, `My Cost per ThruPlay`, `Benchmark Cost per ThruPlay`, `Cost per ThruPlay % Difference`, `My CPE`, `Benchmark CPE`, `CPE % Difference`) VALUES
(1, 'Facebook', 'Lead generation	', 73.28, 41.55, '-76%', 732.93, 322.34, '-127%', 1165.74, 1178.61, '0%', 1.88, 0.75, '151%', 486.66, 70.89, '-587%', 38.67, 0, 'NA'),
(2, 'Facebook', 'Reach', 13.59, 0, '', 28.37, 0, '', 3723.92, 0, '', 0.37, 0, '', 0, 0, '', 6.47, 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lead`
--
ALTER TABLE `lead`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lead`
--
ALTER TABLE `lead`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
