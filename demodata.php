<?php
$link = mysqli_connect("localhost", "root", "", "insights");
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
$sql="SELECT * FROM lead where id=1";

function insigts($X,$Y,$type){
    if($X==0 || $Y==0 || $X=="NA" || $Y=="NA"){
        $Z="NA";
    }else{
        $W=(1-($X/$Y))*100;
        $Z=number_format($W,0)."%";
    }
    switch($type){
       case "CPC":
        if($X > $Y || $X==0 || $Y==0 ){
            return "Your CPC is greater than benchmark CPC by $Z"; 
        }
        elseif($X < $Y || $X==0 || $Y==0 ){
            return "Your CPC is lower than benchmark CPC by $Z";
        }
        case"CPM":
        if($X > $Y || $X==0 || $Y==0 ){
            return "Your CPM is greater than benchmark CPM by $Z"; 
        }
        elseif($X < $Y || $X==0 || $Y==0 ){
            return "Your CPM is lower than benchmark CPM by $Z";
        }
        case"CPL":
        if($X > $Y || $X==0 || $Y==0 ){
            return "Your CPL is greater than benchmark CPL by $Z"; 
        }
        elseif($X < $Y || $X==0 || $Y==0 ){
            return "Your CPL is lower than benchmark CPL by $Z";
        }
        case"CTR":
        if($X > $Y || $X==0 || $Y==0 ){
            return "Your CTR is greater than benchmark CTR by $Z"; 
        }
        elseif($X < $Y || $X==0 || $Y==0 ){
            return "Your CTR is lower than benchmark CTR by $Z";
        }
        case"CostperThruPlayBench":
        if($X > $Y || $X==0 || $Y==0 ){
            return "Your Cost per ThruPlay is greater than benchmark Cost per ThruPlay by $Z"; 
        }
        elseif($X < $Y || $X==0 || $Y==0 ){
            return "Your Cost per ThruPlay is lower than benchmark Cost per ThruPlay by $Z";
        }
        case"CPE":
        if($X > $Y || $X==0 || $Y==0 ){
            return "Your CPE is greater than benchmark CPE by $Z"; 
        }
        elseif($X < $Y || $X==0 || $Y==0 ){
            return "Your CPE is lower than benchmark CPE by $Z";
        }
        case"CPV":
        if($X > $Y || $X==0 || $Y==0 ){
            return "Your CPV is greater than benchmark CPV by $Z"; 
        }
        elseif($X < $Y || $X==0 || $Y==0 ){
            return "Your CPV is lower than benchmark CPV by $Z";
        }
    }
}
if ($result=mysqli_query($link,$sql))
  {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>demo data</title>
    <style>
    table{border-collapse: collapse;border-spacing: 0;}
    table tr th{font-size: 13px;border:1px solid black;}
    </style>
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#btn").click(function(){
                $("#insight").show();
            });
        });
    </script>
</head>
<body>
<table>
    <tbody>
        <tr>
            <td>
                <th>Channel</th>
                <th>Ad Type</th>
                <th>My CPC</th>
                <th>Benchmark CPC</th>
                <th>% Difference</th>
                <th>My CPM</th>
                <th>Benchmark CPM</th>
                <th>% Difference</th>
                <th>My CPL</th>
                <th>Benchmark CPL</th>
                <th>% Difference</th>
                <th>My CTR</th>
                <th>Benchmark CTR</th>
                <th>% Difference</th>
                <th>My Cost per ThruPlay</th>
                <th>Benchmark Cost per ThruPlay</th>
                <th>% Difference</th>
                <th>My CPE</th>
                <th>Benchmark CPE</th>
                <th>% Difference</th>
            </td>
        </tr>
        <tr>
        <?php while ($row=mysqli_fetch_row($result))
            {
                $CPC=$row[3];$CpcBench=$row[4];$cpcPercent=$row[5];
                $CPM=$row[6];$CpmBench=$row[7];$cpmPercent=$row[8];
                $CPL=$row[9];$CplBench=$row[10];$cplPercent=$row[11];
                $CTR=$row[12];$CtrBench=$row[13];$ctrPercent=$row[14];
                $CostperThruPlay=$row[15];$CostperThruPlayBench=$row[16];$CostperThruPlayPercent=$row[17];
                $CPE=$row[18];$CpeBench=$row[19];$cpePercent=$row[20];

                $A=$CPC;$B=$CpcBench;$C=$cpcPercent;
                $D=$CPM;$E=$CpmBench;$F=$cpmPercent;
                $G=$CPL;$H=$CplBench;$I=$cplPercent;
                $J=$CTR;$K=$CtrBench;$L=$ctrPercent;
                $M=$CostperThruPlay;$N=$CostperThruPlayBench;$O=$CostperThruPlayPercent;
                $P=$CPE;$Q=$CpeBench;$R=$cpePercent;
                // $S=$CPV;$T=$CpvBench;$U=$cpvPercent;

                $CPC_message = insigts($A,$B,"CPC");
                $CPM_message = insigts($D,$E,"CPM");
                $CPL_message = insigts($G,$H,"CPL");
                $CTR_message = insigts($J,$K,"CTR");
                $CostperThruPlay_message = insigts($M,$N,"CostperThruPlayBench");
                $CPE_message = insigts($P,$Q,"CPE");
                $CPV_message = insigts($A,$B,"CPV");               
        ?>
            <td>
                <th><?php printf ("%s",$row[1]); ?></th>
                <th><?php printf ("%s",$row[2]); }}?></th>
                <th><?php echo $A;?></th>
                <th><?php echo $B;?></th>
                <th><?php echo $C;?></th>
                <th><?php echo $D;?></th>
                <th><?php echo $E;?></th>
                <th><?php echo $F;?></th>
                <th><?php echo $G;?></th>
                <th><?php echo $H; ?></th>
                <th><?php echo $I; ?></th>
                <th><?php echo $J; ?></th>
                <th><?php echo $K; ?></th>
                <th><?php echo $L; ?></th>
                <th><?php echo $M; ?></th>
                <th><?php echo $N; ?></th>
                <th><?php echo $O; ?></th>
                <th><?php echo $P; ?></th>
                <th><?php echo $Q; ?></th>
                <th><?php echo $R; ?></th>		
            </td>
        </tr>
    </tbody>
</table>
<br>
<br>
<button id="btn">View Insights</button>
<div id="insight" style="display:none;">
    <th><?php echo $CPC_message;?></th><br>
    <th><?php echo $CPM_message;?></th><br>
    <th><?php echo $CPL_message;?></th><br>
    <th><?php echo $CTR_message;?></th><br>
    <th><?php echo $CostperThruPlay_message;?></th><br>
    <th><?php echo $CPE_message;?></th><br>
    <th><?php //echo $CPV_message;?></th><br>
</div>
</body>
</html>
