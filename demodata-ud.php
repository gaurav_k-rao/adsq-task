<?php
$link = mysqli_connect("localhost", "root", "", "tasks_insights");
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}
$sql="SELECT * FROM lead where id=1";


if ($result=mysqli_query($link,$sql))
  {
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>demo data</title>
    <style>
    table{border-collapse: collapse;border-spacing: 0;}
    table tr th{font-size: 13px;border:1px solid black;}
    </style>
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#btn").click(function(){
                $("#insight").show();
            });
        });
    </script>
</head>
<body>
<table>
    <tbody>
        <tr>
            <td>
                <th>Channel</th>
                <th>Ad Type</th>
                <th>My CPC</th>
                <th>Benchmark CPC</th>
                <th>% Difference</th>
                 <th>My CPM</th>
                <th>Benchmark CPM</th>
                <th>% Difference</th>
                <th>My CPL</th>
                <th>Benchmark CPL</th>
                <th>% Difference</th>
                <th>My CTR</th>
                <th>Benchmark CTR</th>
                <th>% Difference</th>
                <th>My Cost per ThruPlay</th>
                <th>Benchmark Cost per ThruPlay</th>
                <th>% Difference</th>
                <th>My CPE</th>
                <th>Benchmark CPE</th>
                <th>% Difference</th>
            </td>
        </tr>
        <tr>
        <?php while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC))
            {
                $A = !empty($row['My CPC']) ? $row['My CPC'] : 'NA' ; // CPC
                $B = !empty($row['Benchmark CPC']) ? $row['Benchmark CPC'] : 'NA'; //CpcBench
                $C = !empty($row['CPC % Difference']) ? $row['CPC % Difference'] : 'NA'; //cpcPercent
                $D = !empty($row['My CPM']) ? $row['My CPM'] : 'NA' ; // CPM
                $E = !empty($row['Benchmark CPM']) ? $row['Benchmark CPM'] : 'NA'; //CPMBench
                $F = !empty($row['CPM % Difference']) ? $row['CPM % Difference'] : 'NA'; //CPMPercent
                $G = !empty($row['My CPL']) ? $row['My CPL'] : 'NA' ; // CPL
                $H = !empty($row['Benchmark CPL']) ? $row['Benchmark CPL'] : 'NA'; //CPLBench
                $I = !empty($row['CPL % Difference']) ? $row['CPL % Difference'] : 'NA'; //CPLPercent
                $J = !empty($row['My CTR']) ? $row['My CTR'] : 'NA' ; // CTR
                $K = !empty($row['Benchmark CTR']) ? $row['Benchmark CTR'] : 'NA'; //CTRBench
                $L = !empty($row['CTR % Difference']) ? $row['CTR % Difference'] : 'NA'; //CTRPercent
                $M = !empty($row['My Cost per ThruPlay']) ? $row['My Cost per ThruPlay'] : 'NA' ; // Cost per ThruPlay
                $N = !empty($row['Benchmark Cost per ThruPlay']) ? $row['Benchmark Cost per ThruPlay'] : 'NA'; //Cost per ThruPlayBench
                $O = !empty($row['Cost per ThruPlay % Difference']) ? $row['Cost per ThruPlay % Difference'] : 'NA'; //Cost per ThruPlayPercent
                $P = !empty($row['My CPE']) ? $row['My CPE'] : 'NA' ; // CPE
                $Q = !empty($row['Benchmark CPE']) ? $row['Benchmark CPE'] : 'NA'; //CPEBench
                $R = !empty($row['CPE % Difference']) ? $row['CPE % Difference'] : 'NA'; //CPEPercent
                // Table formation
                $table = "<td>
                <th>".$row['Channel']."</th>
                <th>".$row['Ad Type']."</th>
                <th>".$A."</th>
                <th>".$B."</th>
                <th>".$C."</th>
                <th>".$D."</th>
                <th>".$E."</th>
                <th>".$F."</th>
                <th>".$G."</th>
                <th>".$H."</th>
                <th>".$I."</th>
                <th>".$J."</th>
                <th>".$K."</th>
                <th>".$L."</th>
                <th>".$M."</th>
                <th>".$N."</th>
                <th>".$O."</th>
                <th>".$P."</th>
                <th>".$Q."</th>
                <th>".$R."</th>  
            </td>";
            echo $table;
            $CPC_message = insigts($A,$B,"CPC");
            $CPM_message = insigts($D,$E,"CPM");
            $CPL_message = insigts($G,$H,"CPL");
            $CTR_message = insigts($J,$K,"CTR");
            $CostperThruPlay_message = insigts($M,$N,"CostperThruPlayBench");
            $CPE_message = insigts($P,$Q,"CPE");
            // $CPV_message = insigts($A,$B,"CPV");
            }
        ?>
        </tr>
    </tbody>
</table>
<br>
<br>
<button id="btn">View Insights</button>
<div id="insight" style="display:none;">
    <p><?php 
    echo $CPC_message;?></p><br>
    <p><?php 
    echo $CPM_message;?></p><br>
    <p><?php 
    echo $CPL_message;?></p><br>
    <p><?php 
    echo $CTR_message;?></p><br>
    <p><?php 
    echo $CostperThruPlay_message;?></p><br>
    <p><?php 
    echo $CPE_message;?></p><br>
</div>
</body>
</html>
<?php } ?>
<?php
function doGetLeadDetails(){
    $res = null;
    $query="SELECT * from demolead";
    $res=mysqli_query(mysqli_connect("localhost", "root", "", "tasks_insights"),$query);
    return $res;
}
function insigts($myData,$benchData,$type){
    $res = doGetLeadDetails();
    if ($res)
    {   
        while ($leadrow=mysqli_fetch_array($res,MYSQLI_ASSOC)){
            // echo "<pre>";
            // print_r($leadrow);
            $formula=!empty($leadrow['formula']) ? $leadrow['formula'] : '';
            $stringData=!empty($leadrow['string']) ? $leadrow['string'] : 'NA';
            if($myData==0 || $benchData==0 || $myData=="NA" || $benchData=="NA"){
                $diffData="NA";
            }else{
                $tempVar=(1-($myData/$benchData))*100;
                $diffData=number_format($tempVar,0)."%";
            }
            switch($type){
                case "CPC":
                   $myInsights = $myData;
                   $benchInsights = $benchData;
                    break;
                case"CPM":
                    $myInsights = $myData;
                   $benchInsights = $benchData;
                    break;
                case"CPL":
                    $myInsights = $myData;
                   $benchInsights = $benchData;
                case"CTR":
                     $myInsights = $myData;
                   $benchInsights = $benchData;
                    break;
                case"CostperThruPlayBench":
                     $myInsights = $myData;
                   $benchInsights = $benchData;
                    break;
                case"CPE":
                     $myInsights = $myData;
                   $benchInsights = $benchData;
                    break;
                case"CPV":
                     $myInsights = $myData;
                   $benchInsights = $benchData;
                    break;

            }
            echo "<br>";
            eval("\$str = \"$stringData\";");
           return $str;
        }
    }
}
?>